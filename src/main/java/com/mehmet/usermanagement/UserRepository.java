package com.mehmet.usermanagement;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

/**
 * @author Mehmet Cetin
 */
public interface UserRepository extends CrudRepository<User, Long> {

}
