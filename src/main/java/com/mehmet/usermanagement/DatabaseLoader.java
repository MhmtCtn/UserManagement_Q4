package com.mehmet.usermanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Mehmet Cetin
 */
@Component
public class DatabaseLoader implements CommandLineRunner {

	private final UserRepository repository;

	@Autowired
	public DatabaseLoader(UserRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception {

		this.repository.save(new User("Mehmet", "Cetin"));
		this.repository.save(new User("Mehmet", "Atilkan"));
		this.repository.save(new User("Seray", "Uzgur"));
	}
}
