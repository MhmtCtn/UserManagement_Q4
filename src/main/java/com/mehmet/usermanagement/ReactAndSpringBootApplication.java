package com.mehmet.usermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Mehmet Cetin
 */
@SpringBootApplication
public class ReactAndSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactAndSpringBootApplication.class, args);
	}
}
