package com.mehmet.usermanagement;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Mehmet Cetin
 */
@Data
@Entity
public class User {

	private @Id @GeneratedValue Long id;
	private String firstName;
	private String lastName;

	private @Version @JsonIgnore Long version;

	private User() {}

	public User(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
}
