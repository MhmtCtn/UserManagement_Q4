module.exports = function follow(api, rootPath, relArray) {
	var root = api({
		method: 'GET',
		path: rootPath
	});

	return relArray.reduce(function(root, arrayItem) {
		return traverseNext(root, arrayItem);
	}, root);

	function traverseNext (root, arrayItem) {
		return root.then(function (response) {

			if(!response.entity._links) {
				return [];
			}

			return api({
				method: 'GET',
				path: response.entity._links[arrayItem].href
			});
		});
	}
};
