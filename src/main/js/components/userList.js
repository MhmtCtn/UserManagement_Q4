'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import User from './user';
/**
 * Class representing the User list.
 * @extends Component
 */
export default class UserList extends Component {
	/**
	 * @constructor
	 */
	constructor(props) {
		super(props);
	}

	render() {
		var users = this.props.users.map(user =>
				<User key={user.entity._links.self.href}
						  user={user}
						  attributes={this.props.attributes}
						  onUpdate={this.props.onUpdate}
						  onDelete={this.props.onDelete}/>
		);

		return (
			<div>
				<table className="table table-striped table-sm">
					<tbody>
						<tr className="table-active">
							<th>First Name</th>
							<th>Last Name</th>
							<th></th>
							<th></th>
						</tr>
						{users}
					</tbody>
				</table>
			</div>
		)
	}
}
