'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

/**
 * Class for the create modal
 * @extends Component
 */
export default class CreateDialog extends Component {
	/**
	 * @constructor
	 */
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	/**
	 * Method to make the submit operation.
	 */
	handleSubmit(e) {
		e.preventDefault();
		var newUser = {};
		this.props.attributes.forEach(attribute => {
			newUser[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onCreate(newUser);
		this.props.attributes.forEach(attribute => {
			ReactDOM.findDOMNode(this.refs[attribute]).value = ''; // clear out the dialog's inputs
		});
		window.location = "#";
	}

	render() {
		var inputs = this.props.attributes.map(attribute =>
			<p key={attribute}>
				<input type="text" placeholder={attribute} ref={attribute} className="field" />
			</p>
		);
		return (
			<div>
				<a href="#createUser" className="button" /*class="text-success"*/>New</a>
				<div id="createUser" className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Create new user</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Create</button>
							<a href="#">Back</a>
						</form>
					</div>
				</div>
			</div>
		)
	}
}
