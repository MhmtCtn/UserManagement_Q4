'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

/**
 * Class for the update modal.
 * @extends Component
 */
export default class UpdateDialog extends Component {
	/**
	 * @constructor
	 */
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	/**
	 * Method to handle the submit operation.
	 */
	handleSubmit(e) {
		e.preventDefault();
		var updatedUser = {};
		this.props.attributes.forEach(attribute => {
			updatedUser[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onUpdate(this.props.user, updatedUser);
		window.location = "#";
	}

	render() {
		var inputs = this.props.attributes.map(attribute =>
				<p key={this.props.user.entity[attribute]}>
					<input type="text" placeholder={attribute}
						   defaultValue={this.props.user.entity[attribute]}
						   ref={attribute} className="field" />
				</p>
		);

		var dialogId = "updateUser-" + this.props.user.entity._links.self.href.slice(-1);

		return (
			<div key={this.props.user.entity._links.self.href}>
				<a href={"#" + dialogId}>Edit</a>
				<div id={dialogId} className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Update an user</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Update</button>
							<a href="#">Back</a>
						</form>
					</div>
				</div>
			</div>
		)
	}

}
