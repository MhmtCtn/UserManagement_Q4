'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import UpdateDialog from './update';
/**
 * User Class
 * @extends Component
 */
export default class User extends Component {
	/**
	 * @constructor
	 */
	constructor(props) {
		super(props);
		this.handleDelete = this.handleDelete.bind(this);
	}
	/**
	 * Method for delete operation.
	 */
	handleDelete() {
		this.props.onDelete(this.props.user);
	}

	render() {
		return (
			<tr>
				<td>{this.props.user.entity.firstName}</td>
				<td>{this.props.user.entity.lastName}</td>
				<td>
					<UpdateDialog user={this.props.user}
								  attributes={this.props.attributes}
								  onUpdate={this.props.onUpdate}/>
				</td>
				<td>
					<button onClick={this.handleDelete} className="btn btn-danger">Delete</button>
				</td>
			</tr>
		)
	}
}
