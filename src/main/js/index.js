'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CreateDialog from './components/create';
import UpdateDialog from './components/update';
import UserList from './components/userList';
import when from 'when';
import client from './util/client';
import follow from './util/follow';

const root = '/api';

/**
 * Class representing the app.
 * @extends Component
 */
class App extends Component {
	/**
	 * @constructor
	 */
	constructor(props) {
		super(props);
		this.state = {users: [], attributes: [], links: {}};
		this.onCreate = this.onCreate.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
	}
	/**
	 * Method to call to get the users.
	 */
	loadFromServer() {
		follow(client, root, ['users']
		).then(userCollection => {
			return client({
				method: 'GET',
				path: userCollection.entity._links.profile.href,
				headers: {'Accept': 'application/schema+json'}
			}).then(schema => {
				this.schema = schema.entity;
				this.links = userCollection.entity._links;
				return userCollection;
			});
		}).then(userCollection => {
			return userCollection.entity._embedded.users.map(user =>
					client({
						method: 'GET',
						path: user._links.self.href
					})
			);
		}).then(userPromises => {
			return when.all(userPromises);
		}).done(users => {
			this.setState({
				users: users,
				attributes: Object.keys(this.schema.properties),
				links: this.links
			});
		});
	}
	/**
	 * Create a new user.
	 * @param {Object} newUser
	 * @param {string} newUser.firstName
	 * @param {string} newUser.lastName
	 */
	onCreate(newUser) {
		var self = this;
		follow(client, root, ['users']).then(response => {
			return client({
				method: 'POST',
				path: response.entity._links.self.href,
				entity: newUser,
				headers: {'Content-Type': 'application/json'}
			})
		}).then(response => {
			return follow(client, root, ['users']);
		}).done(response => {
			if (typeof response.entity._links.last != "undefined") {
				this.onNavigate(response.entity._links.last.href);
			} else {
				this.onNavigate(response.entity._links.self.href);
			}
		});
	}
	/**
	 * Update the user.
	 * @param {Object} currentUser
	 * @param {Object} updatedUser
	 */
	onUpdate(user, updatedUser) {
		client({
			method: 'PUT',
			path: user.entity._links.self.href,
			entity: updatedUser,
			headers: {
				'Content-Type': 'application/json',
				'If-Match': user.headers.Etag
			}
		}).done(response => {
			this.loadFromServer();
		}, response => {
			if (response.status.code === 412) {
				alert('DENIED: Unable to update ' +
					user.entity._links.self.href + '. Your copy is stale.');
			}
		});
	}
	/**
	 * Delete the user.
	 * @param {Object} User
	 */
	onDelete(user) {
		client({
			method: 'DELETE',
			path: user.entity._links.self.href
		}).done(response => {
			this.loadFromServer();
		});
	}
	/**
	 * Navigate to URI
	 * @param {string} navigationURI
	 */
	onNavigate(navUri) {
		client({
			method: 'GET',
			path: navUri
		}).then(userCollection => {
			this.links = userCollection.entity._links;

			return userCollection.entity._embedded.users.map(user =>
					client({
						method: 'GET',
						path: user._links.self.href
					})
			);
		}).then(userPromises => {
			return when.all(userPromises);
		}).done(users => {
			this.setState({
				users: users,
				attributes: Object.keys(this.schema.properties),
				links: this.links
			});
		});
	}
	/**
	 * Load the data on start
	 */
	componentDidMount() {
		this.loadFromServer();
	}

	render() {
		return (
			<div>
				<CreateDialog attributes={this.state.attributes} onCreate={this.onCreate}/>
				<UserList users={this.state.users}
							  links={this.state.links}
							  attributes={this.state.attributes}
							  onUpdate={this.onUpdate}
							  onDelete={this.onDelete}/>
			</div>
		)
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)
