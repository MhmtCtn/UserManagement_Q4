A Maven based REST project that uses Spring Boot and Spring Data for backend and ReactJS for frontend.

Pre-Loading the demo

To work with this application, you need to pre-load it with some data like that
in the DatabaseLoader class.
This class is marked with Spring’s @Component annotation so that it is automatically picked up by @SpringBootApplication.
It implements Spring Boot’s CommandLineRunner so that it gets run after all the beans are created and registered.
It uses constructor injection and autowiring to reach the User Repository.
The run() method is invoked with command line arguments, loading up the data.

Launching the project

To launch the project type ./mvnw spring-boot:run on the command line. (mvnw.bat for Windows users).
This will invoke both backend and frontend.

Then you can reach it at localhost:8080/api on any browser.
